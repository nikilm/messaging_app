import React, {Component} from 'react';
import Message from './Message';
import './Messagebox.css'


import _ from 'lodash';

class MessageList extends Component {
  constructor(props){
    super(props);

    this.state = {
      messages: [],
      name:'',
      userName:'',
      groupName:'',
      try:''
    };
    let group=this.props.group
    
    let app = this.props.db.database().ref(group);
    app.on('value', snapshot => {
     
      let messagesVal=snapshot.val();
      let messages = _(messagesVal)
                      .keys()
                      .map(messageKey => {
                          let cloned = _.clone(messagesVal[messageKey]);
                          cloned.key = messageKey;
                          return cloned;
                      })
                      .value();
      this.setState({
        messages: messages
      });
    });
  }
  
  render() {

   console.log('mailid',this.props.user)

    let messageNodes = this.state.messages.map((message) => {
      let emailid=message.message.split("++??*:;:;*??++")[0]
      console.log(emailid);
      let mess=message.message.split("++??*:;:;*??++")[1]
      
        if(emailid!==this.props.user){
          var time=message.message.split("++??*:;:;*??++")[2].split(' ')[0]
          var date=message.message.split("++??*:;:;*??++")[2].split(' ')[1]
          var time_n_date=time+'  On '+date;
          return (
                    <div className="field">
                    
                        <div class="triangle-isosceles left ">
                           <a> <Message message = {message.message.split("++??*:;:;*??++")[0]} /></a>
                              <Message message = {message.message.split("++??*:;:;*??++")[1]} />
                              <font className="timestamp" size="1"><Message message = {time_n_date} /></font>                            
                        </div>
                    </div>
          )
        }
        else{
          var time=message.message.split("++??*:;:;*??++")[2].split(' ')[0]
          var date=message.message.split("++??*:;:;*??++")[2].split(' ')[1]
          var time_n_date=time+'  On '+date;
          return (
                    <div className="field">
                        <div class="triangle-isosceles right"> 
                         <Message message = {message.message.split("++??*:;:;*??++")[1]} />
                         <font className="timestamp" size="1"><Message message = {time_n_date} /></font>
                        </div>
                    </div>
          )
        }
    });
    return (
      <div >
           {messageNodes} 
      </div>
    );
  }
}

export default MessageList

     